#include <stdio.h>
#include <conio.h>
#include <string.h>

#define SIZE 80


void PrintString(char* str) //процедура печати строки по центру экрана
{
    int len=strlen(str); //узнаем длину строки
    int i;
    int j=(SIZE-len)/2;// j - колво пробелов

    for(i=0;i<j;i++)//печать j пробелов
        printf(" ");
    
    printf("%s",str);//печать строки

    for(i=j+len;i<SIZE;i++)//печать пробелов до конца экрана
        printf(" ");
}

int main()
{
    char str[SIZE];//описание строки

    printf("Enter string:\n");

    gets(str);//ввод строки

    PrintString (str); //печать
    
    return 0;
}

