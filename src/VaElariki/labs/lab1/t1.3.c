// R = D * пи / 180    Где D — угол в градусах, R — угол в радианах.
// D = R * 180 / пи 

 #include <stdio.h>
 #define PI 3.14

int main (void)
{
	float i;
	
	char j;
	  	
	printf("Enter the angle value in degrees(D) or radians(R):\n");

	scanf ("%f%c", &i, &j);
	
	switch (j)
	{
		case 'R': 
		case 'r':
		
	    printf("%.2fD\n", i * 180/PI);

	    break;

	    case 'D':
	    case 'd':
	    
	    printf("%.2fR\n", i * PI/180 );
	    break;

	    default:
	    printf("Enter the unit of measurement");

	}

	return 0;
}

